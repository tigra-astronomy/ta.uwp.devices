﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: SemanticVersionAttribute.cs Last modified: 2015-11-14@02:45 by Tim Long

using System;
using System.Diagnostics.Contracts;
using System.Runtime.Serialization;

namespace TA.UWP
{
    /// <summary>
    ///     Semantic Version attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly)]
    [DataContract]
    public sealed class SemanticVersionAttribute : Attribute
    {
        public SemanticVersionAttribute(string semanticNumber)
        {
            Contract.Requires(!string.IsNullOrEmpty(semanticNumber));
            Contract.Ensures(Version != null);
            Version = new SemanticVersion(semanticNumber);
        }

        [DataMember]
        public SemanticVersion Version { get; }
    }
}