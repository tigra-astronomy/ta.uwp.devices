// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MaybeExtensions.cs Last modified: 2015-11-14@02:45 by Tim Long

using System.Linq;

namespace TA.UWP.ExtensionMethods
{
    public static class MaybeExtensions
    {
        public static bool None<T>(this Maybe<T> maybe)
        {
            if (maybe == null || maybe == Maybe<T>.Empty)
                return true;
            return !maybe.Any();
        }
    }
}