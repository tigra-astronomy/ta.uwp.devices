// This file is part of the TA.UWP.IoTUtilities project
// 
// Copyright � 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: GlobalAssemblyInfo.cs  Last modified: 2015-11-12@22:04 by Tim Long

using System;
using System.Reflection;
using System.Runtime.InteropServices;
using TA.UWP;

[assembly: AssemblyConfiguration("uncontrolled")]
[assembly: AssemblyCompany("Tigra Astronomy")]
[assembly: AssemblyProduct("Universal Windows Platform Core Types")]
[assembly: AssemblyCopyright("Copyright � 2015 Tigra Astronomy, all rights reserved")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

// Version attributes will be patched at build time on the build server

[assembly: AssemblyVersion("0.0.0.0")]
[assembly: AssemblyFileVersion("0.0.0.0")]
[assembly: SemanticVersion("0.0.0-uncontrolled")]