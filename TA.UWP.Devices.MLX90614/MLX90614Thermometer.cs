﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MLX90614Thermometer.cs Last modified: 2015-11-14@04:37 by Tim Long

using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;
using Windows.UI.Xaml;
using TA.UWP.IoTUtilities;
using TA.UWP.IoTUtilities.Builders;
using TA.UWP.IoTUtilities.ExtensionMethods;

namespace TA.UWP.Devices
{
    /// <summary>
    ///     A device driver for the Melexis MLX90614 contactless temperature sensor. Models a device with 3 channels
    ///     (ambient, field 1 and field 2) although not all device variants have all of the channels.
    /// </summary>
    public sealed class MLX90614Thermometer : ITemperatureSensor, INotifySensorValueChanged, IPeriodicSampling
    {
        private const byte ControlRegisterReset = 0x04;
        private const int ControlRegisterAutoIncrementBit = 2;
        public const int NumberOfChannels = 3;
        private const int DefaultSlaveAddress = 0x5a;
        private static readonly Octet ControlRegisterChannelMask = 0xFC;
        private readonly double[] ChannelInstantaneousValues;
        private readonly MovingAverage[] ChannelMovingAverage;
        private readonly double[] ChannelReferenceValues;
        private readonly SynchronizationContext syncContext;
        private readonly II2CDevice target;
        private Maybe<DispatcherTimer> sampleTimer = Maybe<DispatcherTimer>.Empty;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PCF8591AnalogueToDigitalConverter" /> class.
        /// </summary>
        /// <param name="target">
        ///     The I2C controller to be used. A suitable instance can be obtained from
        ///     <see cref="GetFirstI2CDevice" />.
        /// </param>
        public MLX90614Thermometer(II2CDevice target, uint movingAverageWindow = 2)
        {
            Contract.Requires(target != null);
            Contract.Requires(
                target.ConnectionSettings.BusSpeed == I2cBusSpeed.StandardMode,
                "Bus mode not supported. MLX90614 devices support StandardMode bus speed (up to 100KHz).");
            Contract.Requires(movingAverageWindow >= 2);
            this.target = target;

            syncContext = SynchronizationContext.Current; // Capture the creating thread's synchronization context.
            ChannelMovingAverage = new MovingAverage[NumberOfChannels];
            for (var i = 0; i < NumberOfChannels; i++)
                ChannelMovingAverage[i] = new MovingAverage((int) movingAverageWindow);
            ChannelInstantaneousValues = new double[NumberOfChannels];
            ChannelReferenceValues = new double[NumberOfChannels];
            ValueChangedThreshold = Precision;
        }

        /// <summary>
        ///     Gets or sets the offset, which is added to readings after they have been obtained and scaled. The default
        ///     offset is 0.0 to yield native readings in Kelvin. To obtain readings in Celsius, set an offset of -273.15; to
        ///     obtain Farenheit use an offset of -459.67 and also set a <see cref="Scale" /> of 1.8.
        /// </summary>
        /// <value>The offset.</value>
        public double Offset { get; set; } = 0.0;


        /// <summary>
        ///     Gets the precision of the thermometer in Kelvin; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        public double Precision => 0.02;

        public event EventHandler<SensorValueChangedEventArgs> ValueChanged;

        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the <see cref="ValueChanged" />
        ///     event.
        ///     The default value for this property is determined by the device's precision and is implementation specific.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold { get; set; }

        /// <summary>
        ///     Starts automatic periodic sampling. <see cref="ValueChanged" /> events will be raised whenever a channel's value
        ///     changes.
        /// </summary>
        /// <param name="interval">The interval.</param>
        public void StartPeriodicSampling(Timeout interval)
        {
            StopPeriodicSampling();
            var timer = new DispatcherTimer();
            timer.Interval = interval;
            timer.Tick += HandleSampleTimerTick;
            timer.Start();
            sampleTimer = new Maybe<DispatcherTimer>(timer);
        }

        /// <summary>
        ///     Stops automatic periodic sampling.
        /// </summary>
        public void StopPeriodicSampling()
        {
            if (sampleTimer.Any())
            {
                sampleTimer.Single().Stop();
                sampleTimer.Single().Tick -= HandleSampleTimerTick;
                sampleTimer = Maybe<DispatcherTimer>.Empty;
            }
        }

        /// <summary>
        ///     Gets or sets the scale factor that is applied to readings as they are obtained.
        ///     The default scale for a newly created instance is 1.0, which yields readings directly in Kelvin or, with an
        ///     appropriate <see cref="Offset" />, in Celsius.
        ///     To obtain Farenheit, use a scale of 1.8 and an <see cref="Offset" /> of -459.67.
        /// </summary>
        /// <value>
        ///     The scale -
        /// </value>
        public double Scale { get; set; } = 1.0;

        /// <summary>
        ///     Gets or clears the moving average value of the specified channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>The moving average value of the channel taken over the population size specified in the constructor.</returns>
        public double this[uint channel] => Math.Min(Scale, Math.Max(0, ChannelMovingAverage[channel].Average));

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        public Task Reset()
        {
            for (var i = 0; i < NumberOfChannels; i++)
            {
                ChannelInstantaneousValues[i] = 0;
                ChannelMovingAverage[i].Clear();
            }
            return Task.CompletedTask;
        }

        /// <summary>
        ///     sample all channels as an asynchronous operation.
        /// </summary>
        public async Task SampleAllChannelsAsync()
        {
            await Task.Run(() => SampleAllChannels());
        }

        private async void HandleSampleTimerTick(object sender, object e)
        {
            try
            {
                sampleTimer.Single().Stop(); // We don't want re-entrant timer events.
                await SampleAllChannelsAsync();
            }
            catch (TimeoutException ex)
            {
                // ToDo: add logging
                // We ignore timeouts when doing auto-sampling as we don't want to bring down the app just because the sensor missed 1 reading.
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally {
                sampleTimer.Single().Start();
            }
        }

        private void RaiseValueChanged(uint channel, double value)
        {
            var eventArgs = new SensorValueChangedEventArgs(channel, value);
            ValueChanged?.Invoke(this, eventArgs);
        }


        /// <summary>
        ///     Gets the first available I2C controller on the system and configures it for communication with the PCF8591
        ///     A-to-D converter.
        /// </summary>
        /// <param name="address">
        ///     The 7-bit I2C slave address of the target device. Don't get clever and try to allow for the R/W bit; I2C
        ///     addresses should be in the range 0x00 to 0x7F. The PCF8591 accepts addresses in the format <c>1 0 0 1 x x x</c>
        ///     where bits 2, 1 and 0 are set by the hardware implementation (sometimes these are configurable by jumpers or
        ///     solder pads on the PCB). This leaves a possible range of addresses from 0x48 to 0x4F, with the default being
        ///     0x48 is not specified.
        /// </param>
        /// <returns>A configured <see cref="I2cDevice" />.</returns>
        public static async Task<TestableI2CDevice> GetFirstI2CDevice(int address = DefaultSlaveAddress)
        {
            Contract.Requires(
                address >= 0 && address <= 0x7F, "I2C slave addresses are 7-bit integers in the range 0x00 to 0x7F.");
            Contract.Ensures(Contract.Result<TestableI2CDevice>() != null);
            var settings = new I2cConnectionSettings(address);
            settings.BusSpeed = I2cBusSpeed.StandardMode; // 100KHz max bus speed per data sheet
            settings.SharingMode = I2cSharingMode.Shared;
            var aqs = I2cDevice.GetDeviceSelector();
            var devices = await DeviceInformation.FindAllAsync(aqs);
            var WindowsI2CDevice = await I2cDevice.FromIdAsync(devices.First().Id, settings);
            return new TestableI2CDevice(WindowsI2CDevice);
        }

        /// <summary>
        ///     Samples all channels and raises <see cref="ValueChanged" /> events as appropriate.
        /// </summary>
        public void SampleAllChannels()
        {
            for (uint i = 0; i < NumberOfChannels; i++)
            {
                var builder = new SimpleTransactionBuilder().Append((byte) (0x06 + i)).ExpectResponse(3);
                var transaction = target.Transact(builder);
                if (transaction.Result.Status != I2cTransferStatus.FullTransfer)
                {
                    throw new TimeoutException(
                        $"I2C operation failed with status {transaction.Result.Status} after transferring {transaction.Result.BytesTransferred} bytes");
                }
                var rxBuffer = transaction.ReceiveBuffer;
                var rawADU = (rxBuffer[1] << 8) + rxBuffer[0];
                // ToDo: compute and validate checksum
                var sample = rawADU * Precision * Scale + Offset;
                ChannelInstantaneousValues[i] = sample;
                ChannelMovingAverage[i].AddSample(sample);
                var newAverageValue = ChannelMovingAverage[i].Average;
                if (Math.Abs(ChannelReferenceValues[i] - newAverageValue) > ValueChangedThreshold)
                {
                    ChannelReferenceValues[i] = newAverageValue;
                    RaiseValueChanged(i, newAverageValue);
                }
            }
        }
    }
}