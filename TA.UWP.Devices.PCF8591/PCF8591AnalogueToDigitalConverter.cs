﻿// This file is part of the TA.UWP.IoTUtilities project
// 
// Copyright © 2015-2015 Tigra Astronomy., all rights reserved.
// 
// File: PCF8591AnalogueToDigitalConverter.cs  Last modified: 2015-11-11@06:53 by Tim Long

using System;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;
using Windows.System.Threading;
using TA.UWP.IoTUtilities;

namespace TA.UWP.Devices
{
    public sealed class PCF8591AnalogueToDigitalConverter 
        : IAnalogueToDigitalConverter, INotifySensorValueChanged, IPeriodicSampling
    {
        private const byte ControlRegisterReset = 0x04;
        private const int ControlRegisterAutoIncrementBit = 2;
        public const int NumberOfChannels = 4;
        private static readonly byte[] DeviceReset = {ControlRegisterReset, 0x00};
        private static readonly Octet ControlRegisterChannelMask = 0xFC;
        private readonly double[] ChannelInstantaneousValues;
        private readonly MovingAverage[] ChannelMovingAverage;
        private readonly double[] ChannelReferenceValues;
        private readonly II2CDevice target;
        private Maybe<ThreadPoolTimer> sampleTimer = Maybe<ThreadPoolTimer>.Empty;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PCF8591AnalogueToDigitalConverter" /> class.
        /// </summary>
        /// <param name="target">
        ///     The I2C controller to be used. A suitable instance can be obtained from
        ///     <see cref="GetFirstI2CDevice" />.
        /// </param>
        public PCF8591AnalogueToDigitalConverter(II2CDevice target, int movingAverageWindow = 3)
        {
            Contract.Requires(target != null);
            Contract.Requires(target.ConnectionSettings.BusSpeed == I2cBusSpeed.StandardMode,
                "Bus mode not supported. PCF8591 devices support StandardMode bus speed (up to 100KHz).");
            Contract.Requires(movingAverageWindow > 1);
            this.target = target;
            ChannelMovingAverage = new MovingAverage[NumberOfChannels];
            for (var i = 0; i < NumberOfChannels; i++)
            {
                ChannelMovingAverage[i] = new MovingAverage(movingAverageWindow);
            }
            ChannelInstantaneousValues = new double[NumberOfChannels];
            ChannelReferenceValues = new double[NumberOfChannels];
            ValueChangedThreshold = Precision;
        }

        private Octet ControlRegisterShadow { get; set; }

        private uint MaximumADU => (uint) (1 << (int) PrecisionInBits);

        /// <summary>
        ///     Gets or sets the scale.
        /// </summary>
        /// <value>
        ///     The scale - samples are multiplied by the scale as they are obtained so that the moving average
        ///     values are also scaled. The default scale for a newly created instance is 1.0
        /// </value>
        public double Scale { get; set; } = 1.0;

        /// <summary>
        /// Occurs whenever any channel's value has changed, either in response to a manual sampling request or when automatic sampling is enabled.
        /// </summary>
        public event EventHandler<SensorValueChangedEventArgs> ValueChanged;

        /// <summary>
        ///     Gets or clears the moving average value of the specified channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>The moving average value of the channel taken over the population size specified in the constructor.</returns>
        public double this[uint channel] => Math.Min(Scale, Math.Max(0, ChannelMovingAverage[channel].Average));

        /// <summary>
        ///     Gets or sets the precision of the A to D converter, in bits; this implies the maximum value that can be obtained
        ///     from the device.
        /// </summary>
        /// <value>The precision in bits.</value>
        public uint PrecisionInBits => 8;


        /// <summary>
        ///     Gets the precision of the A to D converter hardware; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        public double Precision => 1.0 / MaximumADU;

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        public async Task Reset()
        {
            await Task.Run(() => target.Write(DeviceReset));
            ControlRegisterShadow = ControlRegisterReset;
            for (var i = 0; i < NumberOfChannels; i++)
            {
                ChannelInstantaneousValues[i] = 0;
                ChannelMovingAverage[i].Clear();
            }
        }

        /// <summary>
        ///     sample all channels as an asynchronous operation.
        /// </summary>
        public async Task SampleAllChannelsAsync()
        {
            await Task.Run(() => SampleAllChannels());
        }

        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the <see cref="ValueChanged" />
        ///     event.
        ///     The default value for this property is determined by the device's precision and is implementation specific.
        /// </summary>
        /// <value>The threshold.</value>
        public double ValueChangedThreshold { get; set; }

        /// <summary>
        ///     Starts periodic sampling.
        /// </summary>
        /// <param name="interval">The sampling interval.</param>
        public void StartPeriodicSampling(Timeout interval)
        {
            StopPeriodicSampling();
            var timer = ThreadPoolTimer.CreatePeriodicTimer(source => SampleAllChannels(), interval);
            sampleTimer = new Maybe<ThreadPoolTimer>(timer);
        }

        public void StopPeriodicSampling()
        {
            if (sampleTimer.Any())
            {
                sampleTimer.Single().Cancel();
                sampleTimer = Maybe<ThreadPoolTimer>.Empty;
            }
        }

        private void RaiseValueChanged(uint channel, double value)
        {
            var eventArgs = new SensorValueChangedEventArgs(channel, value);
            ValueChanged?.Invoke(this, eventArgs);
        }

        /// <summary>
        ///     Gets the first available I2C controller on the system and configures it for communication with the PCF8591
        ///     A-to-D converter.
        /// </summary>
        /// <param name="address">
        ///     The 7-bit I2C slave address of the target device. Don't get clever and try to allow for the R/W bit; I2C
        ///     addresses should be in the range 0x00 to 0x7F. The PCF8591 accepts addresses in the format <c>1 0 0 1 x x x</c>
        ///     where bits 2, 1 and 0 are set by the hardware implementation (sometimes these are configurable by jumpers or
        ///     solder pads on the PCB). This leaves a possible range of addresses from 0x48 to 0x4F, with the default being
        ///     0x48 is not specified.
        /// </param>
        /// <returns>A configured <see cref="I2cDevice" />.</returns>
        public static async Task<I2cDevice> GetFirstI2CDevice(int address = 0x48)
        {
            Contract.Requires(address >= 0x48 && address <= 0x4F,
                "For PCF8591 devices, the slave address should be in the range 0x48 to 0x4F.");
            Contract.Ensures(Contract.Result<I2cDevice>() != null);
            var settings = new I2cConnectionSettings(address);
            settings.BusSpeed = I2cBusSpeed.StandardMode; // 100KHz max bus speed per data sheet
            settings.SharingMode = I2cSharingMode.Shared;
            var aqs = I2cDevice.GetDeviceSelector();
            var devices = await DeviceInformation.FindAllAsync(aqs);
            var i2cDevice = await I2cDevice.FromIdAsync(devices.First().Id, settings);
            return i2cDevice;
        }

        public void SampleAllChannels()
        {
            var controlRegister = ControlRegisterShadow & ControlRegisterChannelMask;
            byte[] txBuffer = {controlRegister.WithBitSetTo(ControlRegisterAutoIncrementBit, true)};
            var rxBuffer = new byte[5]; // we will discard the first byte returned
            var result = target.WriteReadPartial(txBuffer, rxBuffer);
            if (result.BytesTransferred > 0)
                ControlRegisterShadow = controlRegister;
            if (result.Status != I2cTransferStatus.FullTransfer)
                throw new TimeoutException(
                    $"I2C operation failed with status {result.Status} after transferring {result.BytesTransferred} bytes");
            for (uint i = 0; i < 4; i++)
            {
                var sample = rxBuffer[i + 1] * Precision * Scale;
                ChannelInstantaneousValues[i] = sample;
                ChannelMovingAverage[i].AddSample(sample);
                var newAverageValue = ChannelMovingAverage[i].Average;
                if (Math.Abs(ChannelReferenceValues[i] - newAverageValue) > ValueChangedThreshold)
                {
                    ChannelReferenceValues[i] = newAverageValue;
                    RaiseValueChanged(i, newAverageValue);
                }
            }
        }
    }
}