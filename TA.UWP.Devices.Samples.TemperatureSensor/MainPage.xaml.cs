﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Microsoft.ApplicationInsights;
using TA.UWP.IoTUtilities;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TA.UWP.Devices.Samples.TemperatureSensor
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static readonly DependencyProperty AmbientTemperatureProperty = DependencyProperty.Register(
            "AmbientTemperature", typeof(double), typeof(MainPage), new PropertyMetadata(default(double)));

        public static readonly DependencyProperty Object1TemperatureProperty = DependencyProperty.Register(
            "Object1Temperature", typeof(double), typeof(MainPage), new PropertyMetadata(default(double)));

        public static readonly DependencyProperty Object2TemperatureProperty = DependencyProperty.Register(
            "Object2Temperature", typeof(double), typeof(MainPage), new PropertyMetadata(default(double)));

        private MLX90614Thermometer sensor;
        private TelemetryClient telemetryClient = new TelemetryClient();

        public MainPage()
            {
            InitializeComponent();
            Loaded += InitializeHardware;
            Loaded += InitializeTelemetry;
            }

        private void InitializeTelemetry(object sender, RoutedEventArgs e)
        {
        }

        private async void InitializeHardware(object sender, RoutedEventArgs e)
            {
            await InitializeHardwareAsync();
            SampleButton.IsEnabled = true;
            AutoEnableButton.IsEnabled = true;
            }

        public double AmbientTemperature
            {
            get { return (double)GetValue(AmbientTemperatureProperty); }
            set { SetValue(AmbientTemperatureProperty, value); }
            }

        public double Object1Temperature
            {
            get { return (double)GetValue(Object1TemperatureProperty); }
            set { SetValue(Object1TemperatureProperty, value); }
            }

        public double Object2Temperature
            {
            get { return (double)GetValue(Object2TemperatureProperty); }
            set { SetValue(Object2TemperatureProperty, value); }
            }

        private async Task InitializeHardwareAsync()
            {
            var i2cDevice = await MLX90614Thermometer.GetFirstI2CDevice();
            sensor = new MLX90614Thermometer(i2cDevice);
            await sensor.Reset();
            sensor.Offset = -273.15; // Readings in Celsius
            sensor.ValueChangedThreshold = 0.1;
            sensor.ValueChanged +=
                (sender, args) => this.RunOnDispatcherThreadAsync(() => HandleSensorValueChanged(sender, args));
            }

        private void HandleSensorValueChanged(object sender, SensorValueChangedEventArgs args)
            {
            switch (args.Channel)
                {
                case 0:
                    AmbientTemperature = args.Value;
                    break;
                case 1:
                    Object1Temperature = args.Value;
                    break;
                case 2:
                    Object2Temperature = args.Value;
                    break;
                }
            }

        private void SampleButton_Click(object sender, RoutedEventArgs e)
            {
            sensor.SampleAllChannels();
            }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
            {
            Application.Current.Exit();
            }

        private void AutoEnableButton_Click(object sender, RoutedEventArgs e)
            {
            AutoEnableButton.IsEnabled = false;
            AutoDisableButton.IsEnabled = true;
            SampleButton.IsEnabled = false;
            sensor.StartPeriodicSampling(Timeout.FromMilliseconds(100));
            }

        private void AutoDisableButton_Click(object sender, RoutedEventArgs e)
            {
            sensor.StopPeriodicSampling();
            AutoEnableButton.IsEnabled = true;
            AutoDisableButton.IsEnabled = false;
            SampleButton.IsEnabled = true;
            }
        }
}
