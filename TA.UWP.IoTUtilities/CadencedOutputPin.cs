// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: CadencedOutputPin.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;
using System.Diagnostics.Contracts;
using Windows.Devices.Gpio;
using Windows.System.Threading;
using TA.UWP.IoTUtilities.ExtensionMethods;

namespace TA.UWP.IoTUtilities
{
    /// <summary>
    ///     Provides an easy and asynchronous way to modulate an output pin in a given pattern over time. A typical use
    ///     is to blink an LED with different patterns to indicate different states, or as an 'activity indicator'. The
    ///     user can control the cadence pattern, whether or not it repeats and how fast the pattern plays back.
    /// </summary>
    public class CadencedOutputPin : IDisposable
    {
        private const ulong bitMask = 0x8000000000000000;
        private const string ObjectDisposed = "The object was disposed";
        private const int BitsInCadencePattern = sizeof(ulong) * 8;
        private readonly object cadenceLock = new object();
        private readonly GpioPin pin;
        private readonly int pinNumber;
        private int cadenceIndex;
        private ulong cadencePattern;
        private ThreadPoolTimer cadenceTimer;
        private bool disposed;
        private ulong reloadPattern;

        /// <summary>
        ///     Initializes a new instance of the
        ///     <see cref="CadencedOutputPin" /> class and optionally specifies
        ///     the speed of cadence playback.
        /// </summary>
        /// <param name="pin">
        ///     The pin to be cadenced; it must be already
        ///     configured correctly as an output pin.
        /// </param>
        /// <param name="cyclesPerSecond">
        ///     The number of cadence cycles per
        ///     second. Optional; default 0.25 (4 second cadence).
        /// </param>
        public CadencedOutputPin(GpioPin pin, double cyclesPerSecond = 0.25)
        {
            Contract.Requires(pin != null);
            Contract.Requires(cyclesPerSecond > 0 && cyclesPerSecond * BitsInCadencePattern <= 1000);
            this.pin = pin;
            pinNumber = pin.PinNumber;
            cadencePattern = CadencePattern.SteadyOff;
            var bitsPerSecond = BitsInCadencePattern * cyclesPerSecond;
            var secondsPerBit = 1 / bitsPerSecond;
            var timerTick = Timeout.FromSeconds(secondsPerBit);
            cadenceTimer = ThreadPoolTimer.CreatePeriodicTimer(CadenceUpdate, timerTick);
        }

        /// <summary>
        ///     Sets the output state to OFF and stops updates.
        ///     Note: does not dispose the output pin, you must do that yourself.
        /// </summary>
        public void Dispose()
        {
            if (!disposed)
            {
                disposed = true;
                cadenceTimer?.Cancel();
                cadenceTimer = null;
                GC.SuppressFinalize(this);
            }
        }

        private void CadenceUpdate(object ignored)
        {
            if (disposed)
                return;
            lock (cadenceLock)
                unchecked
                {
                    var state = (cadencePattern & bitMask) != 0;
                    cadencePattern <<= 1;
                    pin.Write(state);
                    if (++cadenceIndex > 63)
                    {
                        cadenceIndex = 0;
                        cadencePattern = reloadPattern;
                    }
                }
        }

        /// <summary>
        ///     Sets the immediate cadence for the output pin and the 'reload' pattern. The cadence pattern is played
        ///     back, then reloaded from the reload pattern. Setting the reload pattern equal to the cadence pattern
        ///     will result in the cadence being repeated until stopped or changed.
        /// </summary>
        /// <param name="pattern">
        ///     The cadence pattern, which takes effect immediately. This can be one of the standard patterns provided
        ///     in
        ///     <see cref="CadencePattern" /> or any <c>ulong</c> value. a bit value of 1 represents a time slot where the
        ///     output is on/true/asserted/Vcc and a 0 bit represents a time slot where the output is
        ///     off/false/negated/ground.
        /// </param>
        /// <param name="reload">
        ///     The reload pattern, which takes effect once the cadence pattern has been played back. Optional; defaults
        ///     to the value specified in <paramref name="pattern" />.
        /// </param>
        /// <exception cref="System.ObjectDisposedException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        /// <remarks>
        ///     One use of the reload pattern is to arrange for an ouput to play a pattern once and then stop (reload = 0).
        ///     Since a new cadence always takes effect immediately, a cadence with the first few bits set would cause an
        ///     immeidate 'flash' followed by steady off. This ability to re-trigger the cadence can be used to great effect
        ///     to make 'activity indicators' for things like disks, network connections, serial port connections and so on.
        /// </remarks>
        public void SetCadence(ulong pattern, ulong reload)
        {
            if (disposed)
                throw new ObjectDisposedException(ObjectDisposed);
            lock (cadenceLock)
            {
                cadencePattern = pattern;
                reloadPattern = reload;
                cadenceIndex = 0;
            }
        }

        public void SetCadence(ulong pattern) { SetCadence(pattern, pattern); }

        public void SetReload(ulong pattern)
        {
            if (disposed)
                throw new ObjectDisposedException(ObjectDisposed);
            reloadPattern = pattern;
        }

        ~CadencedOutputPin() { Dispose(); }
    }
}