﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: TestableI2CDevice.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;
using System.Diagnostics.Contracts;
using Windows.Devices.I2c;

namespace TA.UWP.IoTUtilities
{
    [ContractClassFor(typeof(II2CDevice))]
    internal abstract class I2CDeviceContracts : II2CDevice
    {
        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(!string.IsNullOrEmpty(DeviceId));
            Contract.Invariant(ConnectionSettings != null);
        }

        #region Implementation of II2cDevice

        public void Write(byte[] buffer) { Contract.Requires(buffer != null && buffer.Length > 0); }

        public I2cTransferResult WritePartial(byte[] buffer)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            throw new NotImplementedException();
        }

        public void Read(byte[] buffer) { Contract.Requires(buffer != null && buffer.Length > 0); }

        public I2cTransferResult ReadPartial(byte[] buffer)
        {
            Contract.Requires(buffer != null && buffer.Length > 0);
            throw new NotImplementedException();
        }

        public void WriteRead(byte[] writeBuffer, byte[] readBuffer)
        {
            Contract.Requires(readBuffer != null && readBuffer.Length > 0);
            Contract.Requires(writeBuffer != null && writeBuffer.Length > 0);
        }

        public I2cTransferResult WriteReadPartial(byte[] writeBuffer, byte[] readBuffer)
        {
            Contract.Requires(readBuffer != null && readBuffer.Length > 0);
            Contract.Requires(writeBuffer != null && writeBuffer.Length > 0);
            throw new NotImplementedException();
        }

        public I2cConnectionSettings ConnectionSettings { get; }
        public string DeviceId { get; }

        #endregion
    }

    /// <summary>
    ///     A wrapper around <see cref="I2cDevice" /> that supports testing and mocking.
    /// </summary>
    public class TestableI2CDevice : II2CDevice
    {
        private readonly I2cDevice device;

        public TestableI2CDevice(I2cDevice device)
        {
            Contract.Requires(device != null);
            this.device = device;
        }

        /// <summary>
        ///     Writes data to the inter-integrated circuit (I2C) bus on which the device is connected, based on the bus address
        ///     specified in the I2cConnectionSettings object that you used to create the I2cDevice object.
        /// </summary>
        /// <param name="buffer">
        ///     A buffer that contains the data that you want to write to the I2C device. This data should not
        ///     include the bus address.
        /// </param>
        public void Write(byte[] buffer)
        {
            device.Write(buffer);
        }

        /// <summary>
        ///     Writes data to the inter-integrated circuit (I2C) bus on which the device is connected, and returns information
        ///     about the success of the operation that you can use for error handling.
        /// </summary>
        /// <returns>
        ///     A structure that contains information about the success of the write operation and the actual number of bytes that
        ///     the operation wrote into the buffer.
        /// </returns>
        /// <param name="buffer">
        ///     A buffer that contains the data that you want to write to the I2C device. This data should not
        ///     include the bus address.
        /// </param>
        public I2cTransferResult WritePartial(byte[] buffer)
        {
            return device.WritePartial(buffer);
        }

        /// <summary>
        ///     Reads data from the inter-integrated circuit (I2C) bus on which the device is connected into the specified buffer.
        /// </summary>
        /// <param name="buffer">
        ///     The buffer to which you want to read the data from the I2C bus. The length of the buffer
        ///     determines how much data to request from the device.
        /// </param>
        public void Read(byte[] buffer)
        {
            device.Read(buffer);
        }

        /// <summary>
        ///     Reads data from the inter-integrated circuit (I2C) bus on which the device is connected into the specified buffer,
        ///     and returns information about the success of the operation that you can use for error handling.
        /// </summary>
        /// <returns>
        ///     A structure that contains information about the success of the read operation and the actual number of bytes that
        ///     the operation read into the buffer.
        /// </returns>
        /// <param name="buffer">
        ///     The buffer to which you want to read the data from the I2C bus. The length of the buffer
        ///     determines how much data to request from the device.
        /// </param>
        public I2cTransferResult ReadPartial(byte[] buffer)
        {
            return device.ReadPartial(buffer);
        }

        /// <summary>
        ///     Performs an atomic operation to write data to and then read data from the inter-integrated circuit (I2C) bus on
        ///     which the device is connected, and sends a restart condition between the write and read operations.
        /// </summary>
        /// <param name="writeBuffer">
        ///     A buffer that contains the data that you want to write to the I2C device. This data should
        ///     not include the bus address.
        /// </param>
        /// <param name="readBuffer">
        ///     The buffer to which you want to read the data from the I2C bus. The length of the buffer
        ///     determines how much data to request from the device.
        /// </param>
        public void WriteRead(byte[] writeBuffer, byte[] readBuffer)
        {
            device.WriteRead(writeBuffer, readBuffer);
        }

        /// <summary>
        ///     Performs an atomic operation to write data to and then read data from the inter-integrated circuit (I2C) bus on
        ///     which the device is connected, and returns information about the success of the operation that you can use for
        ///     error handling.
        /// </summary>
        /// <returns>
        ///     A structure that contains information about whether both the read and write parts of the operation succeeded and
        ///     the sum of the actual number of bytes that the operation wrote and the actual number of bytes that the operation
        ///     read.
        /// </returns>
        /// <param name="writeBuffer">
        ///     A buffer that contains the data that you want to write to the I2C device. This data should
        ///     not include the bus address.
        /// </param>
        /// <param name="readBuffer">
        ///     The buffer to which you want to read the data from the I2C bus. The length of the buffer
        ///     determines how much data to request from the device.
        /// </param>
        public I2cTransferResult WriteReadPartial(byte[] writeBuffer, byte[] readBuffer)
        {
            return device.WriteReadPartial(writeBuffer, readBuffer);
        }

        /// <summary>
        ///     Gets the connection settings used for communication with the inter-integrated circuit (I2C) device.
        /// </summary>
        /// <returns>
        ///     The connection settings used for communication with the inter-integrated circuit (I2C) device.
        /// </returns>
        public I2cConnectionSettings ConnectionSettings => device.ConnectionSettings;

        /// <summary>
        ///     Gets the plug and play device identifier of the inter-integrated circuit (I2C) bus controller for the device.
        /// </summary>
        /// <returns>
        ///     The plug and play device identifier of the inter-integrated circuit (I2C) bus controller for the device.
        /// </returns>
        public string DeviceId => device.DeviceId;

        [ContractInvariantMethod]
        private void ObjectInvariant()
        {
            Contract.Invariant(device != null);
        }
    }
}