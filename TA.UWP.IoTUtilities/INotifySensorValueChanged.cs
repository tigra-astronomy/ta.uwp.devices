// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: INotifySensorValueChanged.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;

namespace TA.UWP.IoTUtilities
{
    /// <summary>
    ///     Implementors have the ability to notify subscribers (via the <see cref="ValueChanged" /> event) when a
    ///     sensor value has changed by a configurable amount, specified by the <see cref="ValueChangedThreshold" />
    ///     property.
    /// </summary>
    public interface INotifySensorValueChanged
    {
        /// <summary>
        ///     Gets or sets the amount that a channel's value must change by in order to trigger the
        ///     <see cref="ValueChanged" />event. The default value for this property is implementation specific and
        ///     will typically be related to the precision of the sensor.
        /// </summary>
        /// <value>The threshold.</value>
        double ValueChangedThreshold { get; set; }

        /// <summary>
        ///     Occurs when a sensor channel's value changes by an amount greater then or equal to
        ///     <see cref="ValueChangedThreshold" />. You should not assume that events will be raised on any particular
        ///     thread.
        /// </summary>
        event EventHandler<SensorValueChangedEventArgs> ValueChanged;
    }
}