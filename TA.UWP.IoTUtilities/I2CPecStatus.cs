﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: I2CPecStatus.cs Last modified: 2015-11-14@04:14 by Tim Long

namespace TA.UWP.IoTUtilities
{
    public enum I2CPecStatus
    {
        NotChecked,
        Valid,
        Invalid
    }
}