// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: IPeriodicSampling.cs Last modified: 2015-11-14@04:14 by Tim Long

namespace TA.UWP.IoTUtilities
{
    public interface IPeriodicSampling
    {
        /// <summary>
        ///     Starts sampling the A-to-D channels repeatedly, at the specified interval. Updated samples will be
        ///     reflected in the channel values and the device will raise <see cref="ValueChanged" /> events in response
        ///     to the reading changing beyond the <see cref="ValueChangedThreshold" />.
        /// </summary>
        /// <param name="interval">The interval between samples.</param>
        void StartPeriodicSampling(Timeout interval);

        /// <summary>
        ///     Stops periodic sampling.
        /// </summary>
        void StopPeriodicSampling();
    }
}