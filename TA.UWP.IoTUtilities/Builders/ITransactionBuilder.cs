﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: ITransactionBuilder.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;
using System.Diagnostics.Contracts;

namespace TA.UWP.IoTUtilities.Builders
{
    [ContractClass(typeof(TransactionBuilderContract))]
    public interface ITransactionBuilder
    {
        ITransactionBuilder Append(params byte[] bytes);
        ITransactionBuilder ExpectResponse(int bytes);
        IDeviceTransaction Build();
    }

    [ContractClassFor(typeof(ITransactionBuilder))]
    internal abstract class TransactionBuilderContract : ITransactionBuilder
    {
        #region Implementation of ITransactionBuilder

        public ITransactionBuilder Append(params byte[] bytes)
        {
            Contract.Requires(bytes != null);
            throw new NotImplementedException();
        }

        public ITransactionBuilder ExpectResponse(int bytes)
        {
            Contract.Requires(bytes >= 0);
            throw new NotImplementedException();
        }

        public IDeviceTransaction Build()
        {
            Contract.Ensures(Contract.Result<IDeviceTransaction>() != null);
            throw new NotImplementedException();
        }

        #endregion
    }
}