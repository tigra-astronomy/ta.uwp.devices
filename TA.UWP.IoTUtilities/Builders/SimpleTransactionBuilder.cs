﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: SimpleTransactionBuilder.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Collections.Generic;

namespace TA.UWP.IoTUtilities.Builders
{
    /// <summary>
    ///     A builder pattern for constructing an <see cref="IDeviceTransaction" />. This trivial implementation will
    ///     most likely serve as a base for more device-specific implementations.
    /// </summary>
    public class SimpleTransactionBuilder : ITransactionBuilder
    {
        private readonly List<byte> transmitBuffer = new List<byte>();
        private int expectedResponseSize;

        #region Implementation of ITransactionBuilder

        public ITransactionBuilder Append(params byte[] bytes)
        {
            transmitBuffer.AddRange(bytes);
            return this;
        }

        public ITransactionBuilder ExpectResponse(int bytes)
        {
            expectedResponseSize += bytes;
            return this;
        }

        public IDeviceTransaction Build()
        {
            return new DeviceTransaction(transmitBuffer.ToArray(), new byte[expectedResponseSize]);
        }

        #endregion
    }
}