// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: SensorValueChangedEventArgs.cs Last modified: 2015-11-14@04:14 by Tim Long

using System;

namespace TA.UWP.IoTUtilities
{
    public sealed class SensorValueChangedEventArgs : EventArgs
    {
        public SensorValueChangedEventArgs(uint channel, double value)
        {
            Channel = channel;
            Value = value;
        }

        public uint Channel { get; }
        public double Value { get; }
    }
}