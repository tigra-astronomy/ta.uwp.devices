// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MovingAverage.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Diagnostics.Contracts;

namespace TA.UWP.IoTUtilities
{
    /// <summary>
    ///     Provides a moving average over a number of samples.
    /// </summary>
    public class MovingAverage
    {
        private readonly object sync = new object();
        protected int index;
        private double runningTotal;
        protected double[] samples;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MovingAverage" /> class and sets the sample population size.
        /// </summary>
        /// <param name="numSamples">The number of samples in the moving average window.</param>
        public MovingAverage(int numSamples)
        {
            Contract.Requires(numSamples > 1, "Population size cannot be less than 2");
            samples = new double[numSamples];
            index = 0;
            Clear();
        }

        public double Average => runningTotal / samples.Length;

        /// <summary>
        ///     Adds a sample to the moving average.
        /// </summary>
        /// <param name="value">The value to be added.</param>
        public void AddSample(double value)
        {
            lock (sync)
            {
                runningTotal = runningTotal - samples[index] + value;
                samples[index++] = value;
                index %= samples.Length;
            }
        }

        /// <summary>
        ///     Adds a sample to the moving average.
        /// </summary>
        /// <param name="value">The value to be added.</param>
        public void AddSample(int value) { AddSample((double) value); }

        /// <summary>
        ///     Clears all the samples, either to 0 or the specified value.
        /// </summary>
        /// <param name="initialValue">
        ///     The initial value to set all the samples to, and therefore the moving average value. This can be useful when
        ///     you would like the moving average to start from a value other than zero.
        /// </param>
        public void Clear(double initialValue = 0.0)
        {
            lock (sync)
            {
                for (var i = 0; i < samples.Length; i++)
                    samples[i] = initialValue;
                runningTotal = initialValue * samples.Length;
            }
        }
    }
}