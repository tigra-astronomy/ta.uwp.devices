// This file is part of the TA.UWP.Devices project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: IAnalogueToDigitalConverter.cs Last modified: 2015-11-14@04:14 by Tim Long

using System.Threading.Tasks;

namespace TA.UWP.IoTUtilities
{
    /// <summary>
    ///     A device or sensor that performs analogue to digital conversion
    /// </summary>
    public interface IAnalogueToDigitalConverter
    {
        /// <summary>
        ///     Gets the value of the specified A-to-D channel.
        /// </summary>
        /// <param name="channel">The channel to be read.</param>
        /// <returns>
        ///     The scaled value of the channel.
        /// </returns>
        double this[uint channel] { get; }

        /// <summary>
        ///     Gets or sets the precision of the A to D converter, in bits; this implies the maximum value that can be obtained
        ///     from the device.
        /// </summary>
        /// <value>The precision in bits.</value>
        uint PrecisionInBits { get; }

        /// <summary>
        ///     Gets the precision of the A to D converter hardware; that is, the smallest amount by which two
        ///     consecutive readings can differ. This implementation uses a moving average so the difference between two
        ///     average values can actually be smaller than this, however the true precision is directly determined by
        ///     the bit width of the underlying device.
        /// </summary>
        /// <value>The maximum value.</value>
        double Precision { get; }

        /// <summary>
        ///     Gets or sets the scale. Defaults to 1.0. Readings will vary between 0.0 and Scale.
        /// </summary>
        /// <value>
        ///     The scale - samples are multiplied by the scale as they are obtained so that the moving average
        ///     values are also scaled. The default scale for a newly created instance is 1.0
        /// </value>
        double Scale { get; set; }

        /// <summary>
        ///     Resets and reinitializes any hardware devices and clears all readings to zero.
        /// </summary>
        /// <returns>Task.</returns>
        Task Reset();

        /// <summary>
        ///     sample all channels as an asynchronous operation. This may result in the <see cref="ValueChanged" />
        ///     event being raised for each channel.
        /// </summary>
        Task SampleAllChannelsAsync();
    }
}