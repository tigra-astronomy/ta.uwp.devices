﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: TimeoutTests.cs Last modified: 2015-11-14@02:45 by Tim Long

using System;
using FluentAssertions;
using Xunit;

namespace TA.UWP.Tests
{
    public class TimeoutTests
    {
        [Fact]
        public void TimeoutCreatedFromSecondsHasTheCorrectNumberOfMillisecondsAndTicks()
        {
            var t = Timeout.FromSeconds(61);
            t.Milliseconds.Should().Be(61000, "should be the total milliseconds, not the milliseconds component");
            t.Ticks.Should().Be(61 * TimeSpan.TicksPerSecond);
        }

        [Fact]
        public void TimeoutCreatedFromFractionalSecondsHasTheCorrectNumberOfMillisecondsAndTicks()
        {
            var t = Timeout.FromSeconds(1.5);
            t.Milliseconds.Should().Be(1500, "should be the total milliseconds, not the milliseconds component");
            t.Ticks.Should().Be(1500 * TimeSpan.TicksPerMillisecond);
        }

        [Fact]
        public void TimeoutPlusTimeoutGivesNewTimeoutWithCombinedTotal()
        {
            var a = Timeout.FromSeconds(1);
            var b = Timeout.FromSeconds(2);
            var total = a + b;
            total.Should().NotBeSameAs(a, "timeouts are immutable");
            total.Should().NotBeSameAs(b, "timeouts are immutable");
            total.Ticks.Should().Be(3 * TimeSpan.TicksPerSecond);
        }

        [Fact]
        public void TimeoutFromTimespanHasExpectedValues()
        {
            var hours = 1.5;
            var t = Timeout.FromTimeSpan(TimeSpan.FromHours(hours));
            t.Milliseconds.Should().Be((int) (1000 * 3600 * hours));
            t.Ticks.Should().Be((long) (hours * TimeSpan.TicksPerHour));
        }

        [Fact]
        public void ToStringFormatsLikeTimeSpan()
        {
            var ts = new TimeSpan(1, 2, 3, 4);
            var t = Timeout.FromTimeSpan(ts);
            t.ToString().Should().Be(ts.ToString());
        }

        [Fact]
        public void ImplicitConversionsWorkImplicitly()
        {
            var ts = new TimeSpan(1, 2, 3, 4);
            var t = Timeout.FromTimeSpan(ts);
            var target = new Implicit();
            target.timeSpan = t;
            target.ticks = t;
            target.milliseconds = t;
            target.timeSpan.Should().Be(ts);
            target.ticks.Should().Be(ts.Ticks);
            target.milliseconds.Should().Be((int) ts.TotalMilliseconds);
        }

        private class Implicit
        {
            public int milliseconds;
            public long ticks;
            public TimeSpan timeSpan;
        }
    }
}