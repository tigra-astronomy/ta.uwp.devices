﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MaybeTests.cs Last modified: 2015-11-14@02:45 by Tim Long

using System.Linq;
using FluentAssertions;
using TA.UWP.ExtensionMethods;
using Xunit;

namespace TA.UWP.Tests
{
    public class MaybeTests
    {
        [Fact]
        public void EmptyMaybeIsReferenceEqual()
        {
            var maybe1 = Maybe<int>.Empty;
            var maybe2 = Maybe<int>.Empty;
            maybe1.Should().BeSameAs(maybe2, "Empty instances should be one and the same object");
            maybe1.Should().Equal(maybe2, "Empty instances should compare equal");
        }

        [Fact]
        public void EmptyMaybeHasNoContents()
        {
            var maybe = Maybe<int>.Empty;
            maybe.Any().Should().BeFalse("empty maybes should be empty");
            maybe.None().Should().BeTrue();
        }

        [Fact]
        public void MaybeWithValueContainsOneItem()
        {
            var expected = new object();
            var maybe = new Maybe<object>(expected);
            maybe.Any().Should().BeTrue();
            maybe.Single()
                .Should()
                .BeSameAs(
                    expected,
                    "Maybe with a value should contain exactly one element and it should be the original object");
        }

        [Fact]
        public void NoneExtensionMethodIsOppositeOfAny()
        {
            var empty = Maybe<object>.Empty;
            var full = new Maybe<object>(new object());
            empty.None().Should().BeTrue();
            full.None().Should().BeFalse();
        }

        [Fact]
        public void ToStringProducesExpectedOutput()
        {
            var empty = Maybe<object>.Empty;
            empty.ToString().Should().Be("{no value}");
            var full = new Maybe<int>(1);
            full.ToString().Should().Be("1", "Should format as the single item would format");
        }
    }
}