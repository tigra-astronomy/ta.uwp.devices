﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: SimpleTransactionBuilderTests.cs Last modified: 2015-11-14@04:13 by Tim Long

using System.Linq;
using FluentAssertions;
using TA.UWP.IoTUtilities.Builders;
using Xunit;

namespace TA.UWP.Tests
{
    public class SimpleTransactionBuilderTests
    {
        [Fact]
        public void NewTransactionContainsNoData()
        {
            var builder = new SimpleTransactionBuilder();
            var transaction = builder.Build();
            transaction.TransmitBuffer.Any()
                .Should()
                .BeFalse("transactions with nothing to send should not have a transmit buffer");
            transaction.ReceiveBuffer.Any()
                .Should()
                .BeFalse("transactions with nothing to receive should not have a receive buffer");
        }

        [Fact]
        public void AppendingDataToTheWriteBufferResultsInAWriteOnlyTransaction()
        {
            byte[] ExpectedBytes = {0x01, 0x02};
            var builder = new SimpleTransactionBuilder().Append(ExpectedBytes);
            var transaction = builder.Build();
            transaction.TransmitBuffer.Should()
                .Equal(ExpectedBytes, "the transmit buffer should contain exactly what was appended");
            transaction.ReceiveBuffer.Any()
                .Should()
                .BeFalse("transactions with nothing to receive should not have a receive buffer");
        }
    }
}