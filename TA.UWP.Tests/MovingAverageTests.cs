﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: MovingAverageTests.cs Last modified: 2015-11-14@04:13 by Tim Long

using System;
using FluentAssertions;
using TA.UWP.IoTUtilities;
using Xunit;

namespace TA.UWP.Tests
{
    public class MovingAverageTests
    {
        [Fact]
        public void PopulationSizeLessThanTwoIsInvalid()
        {
            MovingAverage smoothie;
            try {
                smoothie = new MovingAverage(1);
            }
            catch (Exception ex) {
                ex.Message.Should().Contain("Population size");
            }
        }

        [Fact]
        public void ClearingToAValueYieldsAnAverageOfThatValue()
        {
            var smoothie = new MovingAverage(10);
            smoothie.Clear(5.6);
            smoothie.Average.Should()
                .BeApproximately(5.6, 10E-9, "The average of identical values should be that value");
        }

        [Fact]
        public void ClearingToZeroYieldsAnAverageOfZero()
        {
            var smoothie = new MovingAverage(10);
            smoothie.Clear();
            smoothie.Average.Should().Be(0, "The average of 0 is 0");
        }

        [Fact]
        public void AverageOf1To10is5point5()
        {
            var n = 10;
            var samples = new MovingAverage(n);
            for (var i = 1; i <= n; i++)
                samples.AddSample(i);
            var average = samples.Average;
            average.Should().Be((n + 1.0) / 2.0, "The average of the first n nonzero positive integers is (n+1)/2");
        }
    }
}