﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: AssemblyInfo.cs Last modified: 2015-11-14@02:45 by Tim Long

using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("TA.UWP.Tests")]
[assembly: AssemblyDescription("xUnit tests")]
