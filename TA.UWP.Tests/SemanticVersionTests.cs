﻿// This file is part of the TA.UWP.Devices project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: SemanticVersionTests.cs Last modified: 2015-11-14@02:45 by Tim Long

using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using FluentAssertions.Execution;
using TA.UWP.ExtensionMethods;
using Xunit;

namespace TA.UWP.Tests
{
    public class SemanticVersionTests
    {
        public static IEnumerable<object[]> SemVerCollationOrderSampleData
        {
            get
            {
                yield return new object[] {new SemanticVersion("1.0.0-alpha"), new SemanticVersion("1.0.0-alpha.1")};
                yield return new object[] {new SemanticVersion("1.0.0-alpha.1"), new SemanticVersion("1.0.0-beta.2")};
                yield return new object[] {new SemanticVersion("1.0.0-beta.2"), new SemanticVersion("1.0.0-beta.11")};
                yield return new object[] {new SemanticVersion("1.0.0-beta.11"), new SemanticVersion("1.0.0-rc.1")};
                yield return new object[] {new SemanticVersion("1.0.0-rc.1"), new SemanticVersion("1.0.0-rc.1+build.1")}
                    ;
                yield return new object[] {new SemanticVersion("1.0.0-rc.1+build.1"), new SemanticVersion("1.0.0")};
                yield return new object[] {new SemanticVersion("1.0.0"), new SemanticVersion("1.0.0+0.3.7")};
                yield return new object[] {new SemanticVersion("1.0.0+0.3.7"), new SemanticVersion("1.3.7+build")};
                yield return
                    new object[] {new SemanticVersion("1.3.7+build"), new SemanticVersion("1.3.7+build.2.b8f12d7")};
                yield return
                    new object[]
                    {new SemanticVersion("1.3.7+build.2.b8f12d7"), new SemanticVersion("1.3.7+build.11.e0f985a")};
            }
        }

        [Fact]
        public static void ConstructorInitializesBaseVersionNumbers()
        {
            var version = new SemanticVersion("1.2.3");
            version.MajorVersion.Should().Be(1);
            version.MinorVersion.Should().Be(2);
            version.PatchVersion.Should().Be(3);
            version.PrereleaseVersion.None().Should().BeTrue();
            version.BuildVersion.None().Should().BeTrue();
        }

        [Fact]
        public static void ConstructorParsesFullVersionNumber()
        {
            var version = new SemanticVersion("1.2.3-alpha.4+build.567");
            version.MajorVersion.Should().Be(1);
            version.MajorVersion.Should().Be(1);
            version.MinorVersion.Should().Be(2);
            version.PatchVersion.Should().Be(3);
            version.PrereleaseVersion.Single().Should().Be("alpha.4");
            version.BuildVersion.Single().Should().Be("build.567");
        }

        [Fact]
        public static void ConstructorThrowsAnExceptionIfVersionIsInvalid()
        {
            try
            {
                var v = new SemanticVersion("1.abc.3");
                throw new AssertionFailedException("invalid input should throw ArgumentException");
            }
            catch (ArgumentException) {}
        }

        [Fact]
        public static void CompareToComparesTwoSemanticVersionObjects()
        {
            var version1 = new SemanticVersion(1, 0, 0);
            object version2 = new SemanticVersion(1, 0, 0);
            version1.CompareTo(version2).Should().Be(0);
        }

        [Fact]
        public static void MajorVersionIsLessThanOther()
        {
            var version1 = new SemanticVersion(1, 2, 3);
            var version2 = new SemanticVersion(2, 0, 0);
            version1.Should().BeLessThan(version2);
        }

        [Fact]
        public static void MinorVersionIsGreaterThanOther()
        {
            var version1 = new SemanticVersion(1, 2, 0);
            var version2 = new SemanticVersion(1, 1, 0);
            version2.Should().BeLessThan(version1);
        }

        [Fact]
        public static void PatchVersionIsLessThanOther()
        {
            var version1 = new SemanticVersion(1, 1, 3);
            var version2 = new SemanticVersion(1, 1, 4);
            version1.Should().BeLessThan(version2);
        }

        [Fact]
        public static void ReleaseVersionIsGreaterThanPrereleaseVersion()
        {
            var version1 = new SemanticVersion("1.0.0-alpha");
            var version2 = new SemanticVersion(1, 0, 0);
            version1.Should().BeLessThan(version2);
            version1.Should().BeLessThan(version2);
        }

        [Fact]
        public static void SemanticVersionCannotBeComparedToString()
        {
            try
            {
                var version = new SemanticVersion(1, 0, 0);
                version.CompareTo("1.3.0");
                throw new AssertionFailedException("Cannot compare version to a string");
            }
            catch (ArgumentException) {}
        }

        [Fact]
        public static void VersionIsEqualToItself()
        {
            var version = new SemanticVersion(1, 0, 0);
            version.Equals(version).Should().BeTrue();
        }

        [Fact]
        public static void VersionIsNotEqualToNull()
        {
            var version = new SemanticVersion(1, 0, 0);
            (version == null).Should().BeFalse();
            (null == version).Should().BeFalse();
            (null != version).Should().BeTrue();
            (version != null).Should().BeTrue();
            object other = null;
            version.Equals(other).Should().BeFalse();
        }

        [Fact]
        public static void VersionIsNotEqualToString()
        {
            var version = new SemanticVersion(1, 0, 0);
            object versionNumber = "1.0.0";
            version.Equals(versionNumber).Should().BeFalse("versions cannot be compared to strings");
        }

        [Fact]
        public static void VersionIsTheSameAsItself()
        {
            var version = new SemanticVersion(1, 0, 0);
            0.Should().Be(version.CompareTo(version));
            version.Equals(version).Should().BeTrue();
        }

        [Theory]
        [MemberData("SemVerCollationOrderSampleData", typeof(SemanticVersionTests))]
        public void VersionsCollateAccordingToSemVerSpecification(SemanticVersion lesser, SemanticVersion greater)
        {
            lesser.Should().BeLessThan(greater);
        }

        [Fact]
        public static void VersionsAreNotEqual()
        {
            var version1 = new SemanticVersion("1.0.0");
            var version2 = new SemanticVersion("1.0.0-alpha+build.1");
            object version3 = version2;
            (version1 != version2).Should().BeTrue();
            version1.Equals(version3).Should().BeFalse();
        }

        [Fact]
        public static void VersionCannotBeComparedToNull()
        {
            try
            {
                var version1 = new SemanticVersion(1, 0, 0);
                SemanticVersion version2 = null;
                version1.CompareTo(version2);
                throw new AssertionFailedException("Comapre with null should throw");
            }
            catch (ArgumentNullException) {}
        }
    }
}